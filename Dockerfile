FROM electronuserland/builder:wine

RUN curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - && \
  echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list && \
  curl -sL https://deb.nodesource.com/setup_10.x | bash - && \
  apt-get install -y \
  nodejs \
  rsync \
  yarn \
  zip \
  && apt-get autoremove -y && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /modules && cd /modules && yarn add electron jest-junit @aws-amplify/cli && rm -rf `yarn cache dir`

ENV PATH /usr/sbin:/usr/bin:/sbin:/bin:/modules/node_modules/.bin

RUN mkdir -p /project
WORKDIR /project

