### Building React Electron Apps with Docker

This is a container with all the bells and whistles for building a React Electron app.

It includes:
- AWS Amplify CLI
- Electron Build Tools
- Node 10
- Yarn

To access the container:

```bash
docker pull registry.gitlab.com/aifuzz/docker-electron-builder:latest
```
